# Metalab Timeline

## Authors

* [Christian Frisson](https://gitlab.com/ChristianFrisson): development of the interactive timeline using [TimelineJS3](https://timeline.knightlab.com/)
* [Edith Viau](https://gitlab.com/eviau): data collection
* [Emmanuel Durand](https://gitlab.com/paperManu): data collection
* [Nicolas Bouillot](https://gitlab.com/nicobou): data collection

## Data source

https://docs.google.com/spreadsheets/d/1vTbqcnxMorsZtQhwuy8DuYLATSEP9nISMEjSUq3GhNc/

## Installation

### Install [node.js](https://nodejs.org) package manager

On Ubuntu 20.04 LTS:
```
sudo apt install npm
```

### Install node modules

```
npm i
```
#### Build timeline

```
npm run build
```
