import { Timeline } from "@knight-lab/timelinejs";
import "@knight-lab/timelinejs/dist/css/timeline.css";

// https://timeline.knightlab.com/docs/options.html
let options = {
    font: "ubuntu",
    initial_zoom: 1 
}

let timeline = new Timeline(
  "timeline-embed",
  "https://docs.google.com/spreadsheets/d/e/2PACX-1vRcH2KlaZsS_FjAvDqXN1I0nbZqKB1d11BgvOmYUO2uAy5E-M6aZtrfXKREEf9Nwb7uUH9z3KskExBY/pubhtml",
  options
);
